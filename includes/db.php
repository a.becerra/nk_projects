<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Becerra
 * Date: 11/04/2018
 * Time: 12:59
 */

namespace nikken\includes\db;
use SoapClient;

class db
{
    
    protected $username = 'user';
    protected $password = 'pass';
    
    protected $host = 'localhost';
    protected $db = 'database';

    public $Connection;
    protected $Method;

    /**
     * db constructor.
     */
    function __construct($Method = "OPC")
    {

        $this->Method = $Method;
        if ($Method == "MSSQL"){
            /*To use MSSQL it's necessary add php_pdo_sqlsrv.dll and php_sqlsrv.dll*/
            $this->Connection = new \PDO("sqlsrv:Server={$this->host_latam};Database={$this->db_latam};", $this->username, $this->password);
        }
        else if ($Method == "MYSQL"){
            $this->Connection = new \PDO("mysql:host={$this->host_latam};dbname={$this->db_latam}",  $this->username, $this->password);
        }
        else{
            echo "<script>console.warn('No existe la base de datps');</script>";exit;
        }
    }

    function lastInsertId(){

        $LastInsertID = $this->Connection->lastInsertId();

        return $LastInsertID;

    }

    /**
     * Consult something of only one raw in any table
     * @param string $TableName
     * @param string $FieldID
     * @param string $FieldValue
     * @param string $ExtraQuery
     * @param array $QVal
     * @param string $Select
     * @return bool|mixed
     */
    public function TableInfo($TableName = "", $FieldID = "", $FieldValue = "", $ExtraQuery = "", $QVal = array(), $Select = "")
    {

        $Select = $Select ?: "top 1 *";

        if (is_array($Select)) {
            $Select = implode(", ", $Select);
        }

        $FieldID = preg_replace("/[^a-zA-Z0-9_()]/", "", $FieldID);
        $FieldID2 = preg_replace("/[^a-zA-Z0-9_]/", "", $FieldID);
        $TableName = preg_replace("/[^a-zA-Z0-9_]/", "", $TableName);

        if ($TableName && ($FieldID || ($ExtraQuery))) {

            if ($FieldID) {
                $ExtraValues[$FieldID2] = $FieldValue;

                $sth = $this->Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE {$FieldID} = :{$FieldID2} {$ExtraQuery}");

            } else {
                $sth = $this->Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE 1 = 1 {$ExtraQuery}");
            }
            if (isset($ExtraValues)) {
                $QVal = array_merge($ExtraValues, $QVal);
            }

            $sth->execute($QVal);

            return $data = $sth->fetch(\PDO::FETCH_ASSOC);
        }

        return false;

    }

    /**
     * Consult something of several raws in any table
     * @param string $TableName
     * @param string $FieldID
     * @param string $FieldValue
     * @param string $ExtraQuery
     * @param array $QVal
     * @param string $Select
     * @return array|bool
     */
    public function GroupInfo($TableName = "", $FieldID = "", $FieldValue = "", $ExtraQuery = "", $QVal = array(), $Select = "")
    {

        $Select = $Select ?: "*";

        if (is_array($Select)) {
            $Select = implode(", ", $Select);
        }

        $FieldID = preg_replace("/[^a-zA-Z0-9_()]/", "", $FieldID);
        $FieldID2 = preg_replace("/[^a-zA-Z0-9_]/", "", $FieldID);
        $TableName = preg_replace("/[^a-zA-Z0-9_]/", "", $TableName);

        if ($TableName && ($FieldID || ($ExtraQuery))) {

            if ($FieldID) {
                $ExtraValues[$FieldID2] = $FieldValue;
                $sth = $this->Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE {$FieldID} = :{$FieldID2} {$ExtraQuery}");

            } else {

                $sth = $this->Connection->prepare("SELECT {$Select} FROM {$TableName} WHERE 1 = 1 {$ExtraQuery}");

            }
            if (isset($ExtraValues)) {

                if (!isset($QVal)){
                    $QVal = array_merge($ExtraValues, $QVal);

                }
                else{
                    $QVal = $ExtraValues;

                }
            }
            $sth->execute($QVal);
            return  $sth->fetchAll(\PDO::FETCH_ASSOC);
        }

        return false;

    }

    /**
     * Consult something of several raws in any table
     * @param string $TableName
     * @param string $FieldID
     * @param string $FieldValue
     * @param string $ExtraQuery
     * @param array $QVal
     * @param string $Select
     * @return array|bool
     */
    public function Total($TableName = "", $FieldID = "", $FieldValue = "", $ExtraQuery = "", $QVal = array(), $Select = "")
    {

        $Select = $Select ?: "*";

        if (is_array($Select)) {
            $Select = implode(", ", $Select);
        }

        $FieldID = preg_replace("/[^a-zA-Z0-9_()]/", "", $FieldID);
        $FieldID2 = preg_replace("/[^a-zA-Z0-9_]/", "", $FieldID);
        $TableName = preg_replace("/[^a-zA-Z0-9_]/", "", $TableName);

        if ($TableName && ($FieldID || ($ExtraQuery))) {

            if ($FieldID) {
                $ExtraValues[$FieldID2] = $FieldValue;
                $sth = $this->Connection->prepare("SELECT count({$Select}) FROM {$TableName} WHERE {$FieldID} = :{$FieldID2} {$ExtraQuery}");
            } else {
                $sth = $this->Connection->prepare("SELECT count{$Select}) FROM {$TableName} WHERE 1 = 1 {$ExtraQuery}");
            }
            if (isset($ExtraValues)) {
                if (!isset($QVal)){
                    $QVal = array_merge($ExtraValues, $QVal);
                }
                else{
                    $QVal = $ExtraValues;
                }
            }

            $sth->execute($QVal);
            return  $sth->fetchColumn();
        }

        return false;

    }

    /**
     * Insert data in any table
     * @param string $TableName
     * @param array $Fields
     * @return array|bool
     */
    public function Insert($TableName = "", $Fields = array())
    {

        if ($this->Method == "Consult"){
            echo "<script>console.warn('No tiene permisos de Inserción en consulta.')</script>";
            exit;
        }

        if (!is_array($Fields)) {
            return false;
        }

        if ($TableName) {

            $ids = "";
            $QVal = array();
            $ValuesPDO = "";

            foreach ($Fields as $id => $value) {
                $ids .= $id . ",";
                $ValuesPDO .= ":" . $id . ",";
                $QVal[$id] = $value;
            }

            try{
                $ids = trim($ids, ",");
                $ValuesPDO = trim($ValuesPDO, ",");

                $sth = $this->Connection->prepare("INSERT INTO {$TableName}({$ids}) VALUES ($ValuesPDO)");
                $sth->execute($QVal);

                return true;
            }
            catch(PDOException $e) {
                echo $e->getMessage();
            }

        }

        return false;

    }

    /**
     * Update data in any table
     * @param string $TableName
     * @param array $Fields
     * @param string $FieldID
     * @param string $FieldValue
     * @param string $ExtraQuery
     * @param array $QVal
     * @return array|bool
     */
    public function Update($TableName = "", $Fields = array(), $FieldID = "", $FieldValue = "", $ExtraQuery = "", $QVal = array())
    {

        if ($this->Method == "Consult"){
            echo "<script>console.warn('No tiene permisos de Inserción en consulta.')</script>";
            exit;
        }

        if (!is_array($Fields)) {
            return false;
        }

        $FieldID = preg_replace("/[^a-zA-Z0-9_()]/", "", $FieldID);
        $FieldID2 = preg_replace("/[^a-zA-Z0-9_]/", "", $FieldID);
        $TableName = preg_replace("/[^a-zA-Z0-9_]/", "", $TableName);

        if ($TableName && ($FieldID || ($ExtraQuery))) {

            $ValuesPDO = "";

            foreach ($Fields as $id => $value) {
                $ValuesPDO .= $id . " = :" . $id . ",";
                $QVal[$id] = "" . $value . "";
            }

            $ValuesPDO = trim($ValuesPDO, ",");

            if ($TableName && ($FieldID || ($ExtraQuery))) {

                if ($FieldID) {
                    $ExtraValues[$FieldID2] = $FieldValue;
                    $sth = $this->Connection->prepare("UPDATE {$TableName} SET {$ValuesPDO} WHERE {$FieldID} = :{$FieldID2} {$ExtraQuery}");
                } else {
                    $sth = $this->Connection->prepare("UPDATE {$TableName} SET {$ValuesPDO} WHERE 1 = 1 {$ExtraQuery}");
                }

                if (isset($ExtraValues)) {
                    $QVal = array_merge($ExtraValues, $QVal);
                }

                $sth->execute($QVal);

                return $data = $sth->fetchAll(\PDO::FETCH_ASSOC);
            }

        }

        return false;

    }
    
    function getRealIP()
    {

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"]))
        {
            return $_SERVER["HTTP_FORWARDED"];
        }
        else
        {
            return $_SERVER["REMOTE_ADDR"];
        }

    }

    function SapInsert($DataEncrypt = ""){
        try{

            $client = new SoapClient("http://172.18.2.136/wsAccount/Accountws.asmx?WSDL");

            $response = $client->Account_Bank(['data' => $DataEncrypt]);
            $response = (array)$response;
            $response = $response['Account_BankResult'];
            
            return $response;
            
        }catch(SoapFault $fault) {
            echo "Ha ocurrido un error, consulte con el administrador.";
        } catch(Exception $e) {
            echo "Ha ocurrido un error, consulte con el administrador.";
        }catch(Throwable  $t) {
            echo "Ha ocurrido un error, consulte con el administrador.";
        }

    }

}
