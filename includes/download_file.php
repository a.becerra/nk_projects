<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Becerra
 * Date: 03/05/2018
 * Time: 13:36
 */

if (!$_POST['FileId']) {
    return false;
}

$host_insert = '200.66.68.169';
$db_intra = 'NIKKEN_INTRA';
$username_insert = 'latamdevjava';
$password_insert = 'JavD3v$L4t';

$Connection = new \PDO("sqlsrv:Server={$host_insert};Database={$db_intra};", $username_insert, $password_insert);

$sth = $Connection->prepare("SELECT * FROM Bank_ToolsFiles WHERE FileId = :FileId");
$sth->execute(['FileId' => $_POST['FileId']]);

$FileInfo = $sth->fetchAll(\PDO::FETCH_ASSOC);

if (!$FileInfo) {
    return false;
}

$fileName = basename($FileInfo[0]['FileName']);
$fileName = $FileInfo[0]['FilePath'] . $fileName;

if (!file_exists($fileName)) {
    return false;
}

header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=$fileName");
header("Content-Type: application/png");
header("Content-Transfer-Encoding: binary");
readfile($fileName);