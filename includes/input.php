<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Becerra
 * Date: 11/04/2018
 * Time: 12:59
 */

class input
{

    protected $input;

    function __construct()
    {

        $this->input = $_POST;
        if (isset($_GET['cmd'])) {

            $this->cmd = new stelab\includes\cmd\cmd();

            $this->cmd = $this->cmd->cmd(get_class());

            exit;
        }

    }

    /**
     * Save fields nulls
     * @param array $input
     * @return bool|string
     */
    static function validate($input = array())
    {

        $input = json_decode($input);
        $EmptyInput = array();

        foreach ($input as $id => $data) {

            if (!isset($data) || $data == "" || $data == null || $data == "null" || !$data) {
                $EmptyInput[$id]= $data;
            }

        }

        if (count($EmptyInput) > 0){
            $Validation = input::validation($EmptyInput);
            return $Validation;
        }
        else{
            return false;
        }

    }

    /**
     * Send values of fields nulls
     * @param $Validation
     * @return string
     */
    function validation ($Validation){

        $ValidationInputs = "";

        if (isset($Validation)){
            foreach ($Validation as $InputID => $InputValue){
             $ValidationInputs .= '<script>$("#'.$InputID.'").css({"border-color": "red"});</script>';
         }

     }

     return $ValidationInputs;

 }

    /**
     * Send Value Encrypt
     * @param $Encrypt
     * @return string
     */
     static function aes_encrypt($String = ""){

        $plaintext = $String;
        $password = 'password';
        $method = 'aes-256-cbc';

        $password = substr(hash('sha256', $password, true), 0, 32);
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $encrypted = base64_encode(base64_encode(openssl_encrypt($plaintext, $method, $password, OPENSSL_RAW_DATA, $iv)));

        return $encrypted;

    }

    /**
     * Send Value Decrypt
     * @param $Encrypt
     * @return string
     */
    static function aes_decrypt($Encrypt = ""){

        $password = 'password';
        $method = 'aes-256-cbc';

        $password = substr(hash('sha256', $password, true), 0, 32);
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $decrypted = openssl_decrypt(base64_decode(base64_decode($Encrypt)), $method, $password, OPENSSL_RAW_DATA, $iv);

        return $decrypted;

    }

}