/**
 * Send data in function or file in document
 * @param ModuleUrl
 * @param Data
 * @param Alert
 */
function nk_route_get(ModuleUrl, Data = {}, Alert = {}) {

    if (Data['cmd']) {

        $.ajax({
            data: Data,
            url: ModuleUrl,
            type: 'get',
            beforeSend: function () {

            },
            success: function (response) {

                if (response == 1) {
                    $(".alert").addClass("hidden");
                    $('#alert-success').removeClass('hidden')
                    $('#alert-success-text').html("<i class=\"fa fa-check fa-2x\"></i>&nbsp;" + Alert['success']);
                    $("input").css({"border-color": "#ced4da"});
                } else if (response == 2) {
                    $('#alert-danger').removeClass('hidden')
                    $('#alert-danger-text').html("<i class=\"fa fa-exclamation-triangle fa-2x\"></i>&nbsp;Ha ocurrido un error, consulte con el administrador.<br>");
                } else {
                    $('#alert-danger').removeClass('hidden')
                    $('#alert-danger-text').html("<i class=\"fa fa-exclamation-triangle fa-2x\"></i>&nbsp;" + response);
                }
            }
        });

    }

    else {
        console.warn("No se han mandado los parametros solicitados.")
    }

}

/**
 * Send data in any file in document
 * @param ModuleUrl
 * @param Data
 * @param Alert
 */
function nk_route_post(ModuleUrl, Data = {}, Alert = {}) {

    $.ajax({
        url: ModuleUrl,
        contentType: false,
        processData: false,
        data: Data,
        type: 'post',
        beforeSend: function () {

        },
        success: function (response) {

            if (response == 1) {
                $(".alert").addClass("hidden");
                $('#alert-success').removeClass('hidden')
                $('#alert-success-text').html("<i class=\"fa fa-check fa-2x\"></i>&nbsp;" + Alert['success'] + "<br>");
                $("input").css({"border-color": "#ced4da"});
            } else if (response == 2) {
                $('#alert-danger').removeClass('hidden')
                $('#alert-danger-text').html("<i class=\"fa fa-exclamation-triangle fa-2x\"></i>&nbsp;Ha ocurrido un error, consulte con el administrador.<br>");
            }else {
                $('#alert-warning').removeClass('hidden')
                $('#alert-warning-text').html("<i class=\"fa fa-exclamation-triangle fa-2x\"></i>&nbsp;" + response + "<br>");
            }
        }
    });

}

function session_destroy(){
    $.ajax({
        
        url: "/change_control/modules/admin/login.php",
        data: {'cmd':"session_destroy"},
        type: 'get',

        beforeSend: function () {
            location.reload();
        },
        
    });
}

