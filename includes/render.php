<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 22/02/2018
 * Time: 10:01 AM
 */

/**
 * Class render
 */
class render
{



    /**
     * Convert var $_POST in array
     * @param bool $data
     * @return bool
     */
    static function input($data = false)
    {
        if ($data && !is_array($data)) {
            $data = false;
        }

        if (!$data && count($_POST) >= 1) {
            $data = $_POST;
        } else if (!$data) {
            $data = $_GET;
        }

        return $data;
    }

    /**
     * Render data in template
     * @param $document
     * @param $data
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    static function tplrender($document, $data)
    {

    
        try {
            $loader = new Twig_Loader_Filesystem('_views/');
            $twig = new Twig_Environment($loader, []);
            return $twig->render($document, $data);
        } catch (Exception $e) {
            echo $e;
        }


    }

    /**
     * Generate Modal with JS
     * @param string $Title
     * @param array $JS
     * @param bool $Lock
     * @return bool
     */
    static function modal($Title = "", $JS = array(), $Lock = false, $data = array(), $Type = "Basic")
    {

        if ($Type == "Basic") {
            $Type = "Modal-Basic";
        } else if ($Type == "Comment") {
            $Type = "Modal-Comment";
        } else if ($Type == "Confirm") {
            $Type = "Modal-Confirm";
        } else {
            $Type = "Modal-Basic";
        }

        if (!$Title) {
            return false;
        }

        $Lock = $Lock ? " $('#{$Type}').modal({backdrop: 'static',keyboard: false});" : " $(\"#{$Type}\").modal(\"show\");";

        $Scripts = "$('#{$Type}-Title').html('{$Title}');";

        if (isset($JS)) {

            foreach ($JS as $Script) {

                $Scripts .= $Script;

            }

        }

        $Scripts .= $Lock;

        echo "<script>$('.alert').addClass('hidden');{$Scripts}</script>";

        return true;

    }

    /**
     * Generate Modal with JS
     * @param string $Title
     * @param array $JS
     * @param bool $Lock
     * @return bool
     */
    static function modal1($Title = "", $JS = array(), $Lock = false, $data = array())
    {

        if (!$Title) {
            return false;
        }

        $Lock = $Lock ? " $('#Modal-Basic').modal({backdrop: 'static',keyboard: false});" : " $('#Modal-Basic').modal('show')";

        $Scripts = "$('#Modal-Basic-Title').html('{$Title}');";

        if (isset($JS)) {

            foreach ($JS as $Script) {

                $Scripts .= $Script;

            }

        }

        $Scripts .= $Lock;

        $loader = new Twig_Loader_Array(array(
            '/change_control/templates/modals/modals.html' => "hola {{name}}"
        ));
        $twig = new Twig_Environment($loader);

        return $twig->render('/change_control/templates/modals/modals.html', array('name' => 'Fabien'));
        /* try {
             render::tplrender("modals/modals.html", ["name" => "jkasb"]);
         } catch (Twig_Error_Loader $e) {
         } catch (Twig_Error_Runtime $e) {
         } catch (Twig_Error_Syntax $e) {
         }*/


//        return 1;
//        echo "<script>$('.alert').addClass('hidden');{$Scripts}</script>";

//        return true;

    }


}
