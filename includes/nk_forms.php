<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Becerra
 * Date: 17/04/2018
 * Time: 13:18
 */

class nk_forms
{

    /**
     *
     * @param $FieldID
     * @param $FieldValue
     * @return bool|string
     */
    static function setHTML($FieldID,$FieldValue){


      if (!$FieldID || !$FieldValue ){
        return false;
      }

      $Script = "$('#{$FieldID}').html('{$FieldValue}');";
        //$Script = "$('#{$FieldID}').html(\"{$FieldValue}\");";

      return $Script;

    }
    static function html($Tag = "" ,$Attr = [] ,$Html = ""){

      $HtmlAttr = "";

      if (!$Tag  || !$Html){
        return false;
      }

      if (is_array($Attr) && $Attr){

        foreach ($Attr as $id => $value ){
          $HtmlAttr .= $id . " = \"" . "{$value}\"";
                // echo $HtmlAttr;
        }

      }

      $HtmlTag = "<{$Tag} {$HtmlAttr} >{$Html}</{$Tag}>";

      return $HtmlTag;

    }

    static function set_Script($FieldID = "", $Function = "", $Event = "", $Content){

      if (!$FieldID || !$Function || !$Event || !$Content ){
        return false;
      }
      if (is_array($Content)) {
        return false;
      }

      $Script = "$('#{$FieldID}').{$Function}('{$Event}',function(){

        $Content

      });";
      
      //$Script = "$('#{$FieldID}').html(\"{$FieldValue}\");";

      return $Script;

    }

    static function set_function($Function = "",$Values = ""){

      // print_r($Values);exit;
      if (!$Function || !$Values ){
        return false;
      }

      $JS = "";
      $ValuesNum = (int)count($Values)-1;
      $ii = 0;

      if (is_array($Values) && $Values){

        foreach ($Values as $id => $val ){
          if ($ValuesNum == $ii) {
            $JS .= "\"{$val}\"";
          }
          else{
            $JS .= "\"{$val}\",";
          }
          $ii++;
        }
      }

      $Script = "{$Function}({$JS});";
      return $Script;

    }

  }
