<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Becerra
 * Date: 12/04/2018
 * Time: 13:36
 */

namespace stelab\includes\cmd;

class cmd
{

    protected $cmd;
    protected $class;

    /*Send function through $_GET*/
    function cmd($class)
    {

        $obj = new $class();

        if (!empty($_GET['cmd'])) {
            $metodo = $_GET['cmd'];
            if (method_exists($obj, $metodo)) {
                $obj->$metodo();
            } else {
                echo("<script>console.warn('La función no existe, consulte a un administrador.');</script>");
            }
        }

        return $class;

    }

    function cmd2()
    {

        if (!empty($_GET['cmd'])) {

            try {

            } catch (Exception $e) {

                echo("<script>console.warn('La función no existe, consulte a un administrador.');</script>");

            }

        }

    }

}

?>