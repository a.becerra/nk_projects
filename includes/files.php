<?php
/**
 * Created by PhpStorm.
 * User: Alejandro Becerra
 * Date: 17/04/2018
 * Time: 12:01
 */

class files
{

    protected $db;
    private $FilePath;
    private $CallerID;
    private $CallerModule;
    private $CardID;

    function __construct()
    {
        $this->db = new nikken\includes\db\db("Insert");
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->FilePath;
    }

    /**
     * @param mixed $FilePath
     */
    public function setFilePath($FilePath)
    {
        $this->FilePath = $FilePath;
    }

    /**
     * @return mixed
     */
    public function getCardID()
    {
        return $this->CardID;
    }

    /**
     * @param mixed $CallerID
     */
    public function setCardID($CardID)
    {
        $this->CardID = $CardID;
    } 
    /**
     * @return mixed
     */
    public function getCallerID()
    {
        return $this->CallerID;
    }

    /**
     * @param mixed $CallerID
     */
    public function setCallerID($CallerID)
    {
        $this->CallerID = $CallerID;
    }

    /**
     * @return mixed
     */
    public function getCallerModule()
    {
        return $this->CallerModule;
    }

    /**
     * @param mixed $CallerModule
     */
    public function setCallerModule($CallerModule)
    {
        $this->CallerModule = $CallerModule;
    }

    /**
     * Create Path into project
     * @param string $Name
     * @param string $TempName
     * @return array
     */
    function create_file($Name = "", $TempName = "")
    {

        $file_parent = $this->generate_name(5);
        $file_sub = $this->generate_name(2);
        $file_name = $file_parent . "/" . $file_sub;

        $file_path = $this->FilePath . $file_name . "/";

        mkdir($file_path, 0777, true);

        $Target = $file_path . $Name;
        $Source = $TempName;

        move_uploaded_file($Source, $Target);

        $FileID = $this->save($Name,$file_path);

        $result = [$Target,$FileID];

        return $result;

    }

    /**
     * Generate random name
     * @param $length
     * @return bool|string
     */
    function generate_name($length)
    {

        $file_name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
        return $file_name;

    }

    function save($FileName,$Target){

        $InsertFields['DocDate']        = date("Y-m-d H:i:s");
        $InsertFields['CallerID']       = $this->CallerID;
        $InsertFields['CallerModule']   = $this->CallerModule;
        $InsertFields['CardId']         = $this->CardID;
        $InsertFields['FileName']       = $FileName;
        $InsertFields['FilePath']       = $Target;

        $this->db->Insert("Bank_ToolsFiles", $InsertFields);

        $FileID = $this->db->lastInsertId();

        return $FileID;

    }

}
